# doodle-hw

Solution for Tamedia's [data engineer hiring challenge](https://github.com/tamediadigital/hiring-challenges/tree/master/data-engineer-challenge).

## Setup and Run Application

### Download kafka.

```
curl https://www-eu.apache.org/dist/kafka/2.2.0/kafka_2.12-2.2.0.tgz -o kafka_2.12-2.2.0.tgz 
tar -xzf kafka_2.12-2.2.0.tgz
cd kafka_2.12-2.2.0
```

### Start zookeeper and kafka. (in separate console)

```
bin/zookeeper-server-start.sh config/zookeeper.properties
bin/kafka-server-start.sh config/server.properties 
```

### Create kafka topics

```
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic events
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic reports
```

### Get data generator and genereate some data
```
cd ..
git clone https://github.com/tamediadigital/hiring-challenges.git
./hiring-challenges/data-engineer-challenge/data-generator/bin/data-generator-linux -c 1000000 -o stream_1M.json -r 1000 -n 100000 -s 1
```

### Clone this repo
```
git clone https://gitlab.com/gdome13/doodle-hw.git
cd doodle-hw

```

### Setup Virtualenv and download all depencies

If you do not have already python3 and virtualenv installed on your machine, you find a good article [here](https://vitux.com/install-python3-on-ubuntu-and-set-up-a-virtual-programming-environment/).
```
python3 -m venv kafka_venv
. ./kafka_venv/bin/activate
pip install -r requirements.txt
```

### Run Console-producer to ingest log frames

```
cd ../kafka_2.12-2.2.0
cat ../stream_1M.json | bin/kafka-console-producer.sh --broker-list localhost:9092 --topic events 
```

### Run Console-consumer to check output (in separate console)

```
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic reports --from-beginning
```

### Run app to process log frames (in separate console)

```
cd ../doodle-hw
. ./kafka_venv/bin/activate  # Make sure virtualenv is activated.
python src/app.py  # And minute aggregations are arriving in the reports conusmer console.
```

### Examples regarding benchmarking

```
python doodle-hw/src/app.py -d -c HLLC -o stdout
python doodle-hw/src/app.py -d -c HLL -o stdout
python doodle-hw/src/app.py -d -c SET   -o stdout
```

## Notes regarding current implementation

### When to output data

I have chosen the simplest approach according this topic: flush data whenever a minute elapsed since last flush regarding the ts disclosed in the frame. With this approach I can easily process historical data as well, and also if there is an outage, processing can continue where it stopped (in production auto.offset=latest). In the basic solution I do not cover handling bitflips and misordered messages.

### Choosing Kafka client

I have chosen [confluent-kafka](https://github.com/confluentinc/confluent-kafka-python) as the client for processing messages in Python based on the following articles: [1](http://activisiongamescience.github.io/2016/06/15/Kafka-Client-Benchmarking/), [2](http://matthewrocklin.com/blog/work/2017/10/10/kafka-python).

The other two candidates were [kafka-python](https://kafka-python.readthedocs.io/en/master/) and [PyKafka](https://pykafka.readthedocs.io/en/latest/).

Pros:
   * Maintained by [Confluent](https://www.confluent.io), the primary for-profit company that supports and maintains Kafka.
   * Fastest out of the three.
   * Wanted to try out something new (earlier I had some experience with kafka-python).

Cons:
   * Minimal documentation.

### Choosing Data structure for counting unique users

I have chosen [HyperLogLog](https://en.wikipedia.org/wiki/HyperLogLog) for counting unique users in the given timeframe based on the following articles: [1](http://highscalability.com/blog/2012/4/5/big-data-counting-how-to-count-a-billion-distinct-objects-us.html), [2](https://medium.com/@muppal/probabilistic-data-structures-in-the-big-data-world-code-b9387cff0c55).

Python implementation can be found [here](https://github.com/svpcom/hyperloglog) and [here](https://github.com/ascv/HyperLogLog).

Pros:
   * Space efficient: can count one billion distinct items with an accuracy of 2% using only 1.5 kilobytes.

Cons:
   * Approximate alogrithm, but for analytic purposes 1-2% error is acceptable.

### Json vs. Avro

I have chosen to go with the kafka-console-producer and therefore json for simplicity. However I could gain performance improvement with [avro](https://www.confluent.io/blog/avro-kafka-data/). 

Avro pros:
   * Binary format -> fast.
   * Direct mapping to and from json.

Avro cons:
   * Pre defined schema is needed.
   * Kafka-console-producer cannot be used, custom kafka-producer is needed, which creates avro from the log frames. 

Conclusion:
   * In a real-life situation I would send log frames to kafka in avro.

### Command-line paramaters

Helping benchmarking and other features I added some command line parameters. In debug/benchmark mode data can be easily reprocessed without resending it as auto.offset.reset=earliest in that case and consumer group always get a new id.

```
(kafka_venv) ~/hobbydev/doodle/doodle-hw (master) $ python src/app.py -h
usage: app.py [-h] [-c {HLL,HLLC,SET}] [-d] [-o {kafka,stdout}]

optional arguments:
  -h, --help            show this help message and exit
  -c {HLL,HLLC,SET}, --counter {HLL,HLLC,SET}
                        Data structure for counting unique users. (default:
                        HLL)
  -d, --debug           Show debug messages for benchmarking (default: False)
  -o {kafka,stdout}, --output {kafka,stdout}
                        Where to write output. (default: kafka)

```

## Benchmarking

I measured performance on the above mentioned 1M json generated data with set, hyperloglog pure python implementation and hyperloglog C implementation for python.

Results:
   * Set 31 sec
   * HLL(C) 35 sec
   * HLL(python) 37 sec

Which means througput was around 27-33K msg/sec depending on the algorithm used. It can be also seen, that in this size HLL is not worth it as the Set sizes were around 40K in a minute which does not really eat up memory. However on a billion scale it's really worth it because the whole structure takes a few KB instead of GB-s.

## Opportunities for further improvement

### Flexibility to handle different granuralities (min/hour/day)

Solution for this could be a similar logic, but instead of writing out the per minute unique user count, the HyperLogLog object should be serialized and written to Kafka. With the merge feature of Hyperloglog the unique count of the union of multiple minute-batches could be easily given back with one more custom consumer and most probably with the help of some scheduler (e.g cron) in a batch processing way.

### Handle late frames and random timestamps

A possible approach can be to drop those messages which have a "random" timestamp, so does not follow the pattern. I mean in normal cases the current ts should not be out of the prev +/- 5 seconds let's say.

