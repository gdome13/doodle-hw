from abc import ABC, abstractclassmethod
from hyperloglog import HyperLogLog
from HLL import HyperLogLog as HyperLogLogC


HLL_ERR = 0.01
HLLC_REG = 14


class UserCounter(ABC):
    @abstractclassmethod
    def add(self, uid):
        pass

    @abstractclassmethod
    def __len__(self):
        pass


class SetUserCounter(UserCounter):
    def __init__(self):
        self.items = set([])

    def reset(self):
        self.items = set([])

    def add(self, uid):
        self.items.add(uid)

    def __len__(self):
        return len(self.items)


class HllUserCounter(UserCounter):
    def __init__(self):
        self.hll = HyperLogLog(HLL_ERR)

    def reset(self):
        self.hll = HyperLogLog(HLL_ERR)

    def add(self, uid):
        self.hll.add(uid)

    def __len__(self):
        return len(self.hll)


class HllcUserCounter(UserCounter):
    def __init__(self):
        self.hll = HyperLogLogC(HLLC_REG)

    def reset(self):
        self.hll = HyperLogLogC(HLLC_REG)

    def add(self, uid):
        self.hll.add(uid)

    def __len__(self):
        return round(self.hll.cardinality())


def create_usercounter(name):
    if name == 'SET':
        return SetUserCounter()
    elif name == 'HLL':
        return HllUserCounter()
    elif name == 'HLLC':
        return HllcUserCounter()
