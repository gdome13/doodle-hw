import datetime
import json
import random
import sys


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from confluent_kafka import Consumer, KafkaException, Producer
from utils import create_usercounter


BOOTSTRAP_SERVERS = 'localhost:9092'
EVENT_TOPIC = 'events'
REPORT_TOPIC = 'reports'

INTERVALL = 60


def parse_args():
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '-c',
        '--counter',
        choices=['HLL', 'HLLC', 'SET'],
        default='HLL',
        help='Data structure for counting unique users.'
    )
    parser.add_argument(
        '-d',
        '--debug',
        action='store_true',
        help='Show debug messages for benchmarking'
    )
    parser.add_argument(
        '-o',
        '--output',
        choices=['kafka', 'stdout'],
        default='kafka',
        help='Where to write output.'
    )
    return parser.parse_args()


def main():
    args = parse_args()

    if args.debug:
        kafka_consumer_config = {
            'bootstrap.servers': BOOTSTRAP_SERVERS,
            'group.id': 'usercounter_{}'.format(random.randint(100000, 999999)),  # make testing easier
            'auto.offset.reset': 'earliest'

        }
    else:
        kafka_consumer_config = {
            'bootstrap.servers': BOOTSTRAP_SERVERS,
            'group.id': 'usercounter'
        }
    consumer = Consumer(kafka_consumer_config)
    consumer.subscribe([EVENT_TOPIC])

    if args.output == 'kafka':
        producer = Producer({'bootstrap.servers': BOOTSTRAP_SERVERS})

    msg_counter = 0
    start_time = datetime.datetime.now()
    last_flush = None
    uc = create_usercounter(args.counter)

    try:
        while True:
            msg = consumer.poll(1.0)

            if msg is None:
                continue
            if msg.error():
                raise KafkaException(msg.error())

            try:
                value = json.loads(msg.value().decode('utf-8'))
                ts = int(value['ts'])
                uid = value['uid']
            except Exception:
                continue

            msg_counter += 1
            if msg_counter % 1000000 == 0 and args.debug:
                print('{} {}'.format('Elapsed time:', datetime.datetime.now() - start_time))

            # Do not flush on first message
            if last_flush is None:
                last_flush = ts

            # Flush if the given time interval elapsed
            if ts - last_flush >= INTERVALL:
                msg_value = json.dumps({'ts': ts, 'user_count': len(uc)})
                if args.output == 'kafka':
                    producer.produce(topic=REPORT_TOPIC, value=msg_value.encode('utf-8'))
                elif args.output == 'stdout':
                    print(msg_value)
                uc.reset()
                last_flush = ts

            uc.add(uid)

    except KeyboardInterrupt:
        sys.stderr.write('%% Aborted by user\n')
    finally:
        consumer.close()


if __name__ == '__main__':
    main()
